export type Errors<T> = {[P in keyof T]: (string[] | Errors<T[P]>)};

export interface Validation<T> {
    isValid: boolean;
    errors?: Errors<T>;
}
/**
 * ValidationState
 */
export interface ValidationState<T> {
    isValid: boolean;
    error?: string | Errors<T>;
}

export interface Approval<T> {
    isValid: boolean;
    error?: string | Errors<T>;
}

export type RuleAction<T> = (object: T, key: string) => ValidationState<T>;

export type UnArray<T> = T extends (infer U)[] ? U : T;
