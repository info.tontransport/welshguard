import * as _ from 'underscore';

const isNot = (value: boolean): boolean => !value;

const isNull = (value: Object): boolean => value === null;
const isNotNull = (value: Object): boolean => isNot(isNull(value));

const isUndefined = (value: Object): boolean => value === undefined;
const isNotUndefined = (value: Object): boolean => isNot(isUndefined(value));

const isNotDefined = (value: Object): boolean => isNull(value) || isUndefined(value);

/**
 * Valid if the passed value's type is not null or not undefined
 * @param value value to validate
 */
export const isDefined = (value: Object): boolean => isNot(isNotDefined(value));

const isArray = (value: Object): boolean => {
    try {
        _.clone(value as any).pop();
    } catch {
        return false;
    }
    return typeof value === 'object';
};
const isNotArray = (value: Object): boolean => isNot(isArray(value));

const isObject = (value: Object): boolean => typeof value === 'object' && isNotArray(value);
const isNotObject = (value: Object): boolean => isNot(isObject(value));

const isString = (value: Object): boolean => typeof value === 'string';
const isNotString = (value: Object): boolean => isNot(isString(value));

const isNumber = (value: Object): boolean => typeof value === 'number';
const isNotNumber = (value: Object): boolean => isNot(isNumber(value));

const isBoolean = (value: Object): boolean => typeof value === 'boolean';
const isNotBoolean = (value: Object): boolean => isNot(isBoolean(value));

const isEmpty = (value: Object): boolean => isNumber(value) ? isNotDefined(value) : _.isEmpty(value);
const isNotEmpty = (value: Object): boolean => isNot(isEmpty(value));

const regexEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const isEmail = (value: string): boolean => isString(value) && regexEmail.test(value);
const isNotEmail = (value: string): boolean => isNot(isEmail(value));

const regexPhoneNumber = /^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/;
const isPhoneNumber = (value: string): boolean => regexPhoneNumber.test(value);
const isNotPhoneNumber = (value: string): boolean => isNot(isPhoneNumber(value));

export const hasMin = (value: any, min: number): boolean => {
    return isNumber(value) ? value >= min : value.length >= min;
};

export {
    isNot,
    isNull,
    isNotNull,
    isUndefined,
    isNotUndefined,
    isNotDefined,
    isObject,
    isNotObject,
    isArray,
    isNotArray,
    isString,
    isNotString,
    isNumber,
    isNotNumber,
    isBoolean,
    isNotBoolean,
    isEmpty,
    isNotEmpty,
    isEmail,
    isNotEmail,
    isPhoneNumber,
    isNotPhoneNumber,
};
