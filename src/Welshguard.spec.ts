import { NextFunction, Request, Response } from 'express';
import * as _ from 'underscore';
import { getIsRequiredErrorMessage, getMustBeMinThatErrorMessage, getShouldNotBeEmptyErrorMessage } from './Locales';
import { Validation } from './Types';
import { Welshguard } from './Welshguard';
import { ContractType, Validatable } from './PaperValidation';

describe('Welshguard', () => {
    let welshguard: Welshguard;
    beforeEach(() => {
        welshguard = new Welshguard();
    });
    describe('getBodyGuard', () => {
        let fakeRequest: Partial<Request>;
        let fakeResponse: Partial<Response>;
        let fakeNext: Partial<NextFunction>;

        beforeEach(() => {
            fakeRequest = {
                body: {name: 'anthony'},
            };
            const send = jest.fn(_.identity);
            fakeResponse = {
                send,
                status: jest.fn(() => ({send})),
            };
            fakeNext = jest.fn(_.noop);
        });

        it('Should not send a responce if body is valid.', () => {
            welshguard.getBodyGuard({name: {type: ContractType.String}})(
                fakeRequest as Request,
                fakeResponse as Response,
                fakeNext as NextFunction,
            );

            expect(fakeResponse.send).not.toBeCalled();
            expect(fakeNext).toBeCalled();
        });

        it('Should send a responce if body is not valid.', () => {
            welshguard.getBodyGuard({name: {type: ContractType.Object}})(
                fakeRequest as Request,
                fakeResponse as Response,
                fakeNext as NextFunction,
            );

            expect(fakeResponse.send).toBeCalled();
            expect(fakeNext).not.toBeCalled();
        });
    });
    describe('validate', () => {
        it('Should return a valid validation if attr is not required and the value is not defined', () => {
            const toValid: any = {id: null};
            const terms: Validatable<{id: string}> = {id: {type: ContractType.String, notEmpty: true}};

            const validation: Validation<{id: string}> = welshguard.validate(toValid, terms);

            expect(validation.isValid).toBe(true);
            expect(validation.errors).toBe(undefined);
        });

        it('Should return a valid validation', () => {
            const toValid = {
                id: 'string',
                count: 0,
                email: 'test@test.test',
            };
            const terms: Validatable<{id: string}> = {
                id: {required: true, type: ContractType.String/* , notEmpty: true, string: true */},
                // count: {number: true},
                // email: {email: true},
            };

            const validation: Validation<{id: string}> = welshguard.validate(toValid, terms);

            expect(validation.isValid).toBe(true);
            expect(validation.errors).toBe(undefined);
        });

        it('Should return an invalid validation if terms ask an attr to be defined and it isnt', () => {
            const toValid: any = {};
            const terms: Validatable<{id: string}> = {
                id: {required: true, type: ContractType.String},
            };

            const validation: Validation<{id: string}> = welshguard.validate<{id: string}>(toValid, terms);

            expect(validation.isValid).toBe(false);
            expect(validation.errors.id).toEqual([getIsRequiredErrorMessage()]);
        });

        it('Should return a valid validation if terms ask an attr to be not defined and it isnt', () => {
            const toValid: any = {id: 'some thing'};
            const terms: Validatable<{id: string}> = {
                id: {required: false, type: ContractType.String},
            };

            const validation: Validation<{id: string}> = welshguard.validate(toValid, terms);

            expect(validation.isValid).toBe(true);
            expect(validation.errors).toEqual(undefined);
        });

        it('Should return an invalid validation if terms ask an attr to not be empty and it isnt', () => {
            const toValid: any = {id: ''};
            const terms: Validatable<{id: string}> = {
                id: {notEmpty: true, type: ContractType.String},
            };

            const validation: Validation<{id: string}> = welshguard.validate(toValid, terms);

            expect(validation.isValid).toBe(false);
            expect(validation.errors.id).toEqual([getShouldNotBeEmptyErrorMessage('id')]);
        });

        it('Should return an invalid validation if terms ask an attr to be empty and it isnt', () => {
            const toValid: {id: string} = {id: 'some thing'};
            const terms: Validatable<{id: string}> = {
                id: {notEmpty: false, type: ContractType.String},
            };

            const validation: Validation<{id: string}> = welshguard.validate(toValid, terms);

            expect(validation.isValid).toBe(true);
            expect(validation.errors).toEqual(undefined);
        });

        it('Should return an invalid validation if terms ask an attr to have a min and it do not respect it on a number', () => {
            const toValid: any = {
                numberWithMin: 10,
                numberWithoutMin: 0,
            };
            const terms: Validatable<{numberWithMin: number, numberWithoutMin: number}> = {
                numberWithMin: {type: ContractType.Number, min: 10},
                numberWithoutMin: {type: ContractType.Number, min: 10},
            };

            const validation: Validation<{numberWithMin: number, numberWithoutMin: number}> = welshguard.validate(toValid, terms);

            expect(validation.isValid).toBe(false);
            expect(validation.errors.numberWithMin).toBe(undefined);
            expect(validation.errors.numberWithoutMin).toEqual([getMustBeMinThatErrorMessage(10)]);
        });

        it('Should return an invalid validation if terms ask an attr to have a min and it do not respect it on a string', () => {
            type Test = {varMin: string, varNoMin: string};
            const min: number = 5;
            const toValid: Test = {
                varMin: 'abcdeii',
                varNoMin: 'ab',
            };
            const terms: Validatable<Test> = {
                varMin: {type:  ContractType.String, min},
                varNoMin: {type: ContractType.String, min},
            };

            const validation: Validation<Test> = welshguard.validate(toValid, terms);

            expect(validation.isValid).toBe(false);
            expect(validation.errors.varMin).toBe(undefined);
            expect(validation.errors.varNoMin).toEqual([getMustBeMinThatErrorMessage(min)]);
        });

        it('Should return a valid validation if terms ask an object to has an specific structur and it respect', () => {
            const varObject: string = 'valObject';
            const toValid: any = {[varObject]: {id: 'some string'}};
            const subTerms: Validatable<{id: string}> = {id: {required: true, notEmpty: true, type: ContractType.String}};
            const terms: Validatable<{object: {id: string}}> = {object: {type:  ContractType.Object, content: subTerms}};

            const validation: Validation<{object: {id: string}}> = welshguard.validate(toValid, terms);

            expect(validation.isValid).toBe(true);
            expect(validation.errors).toBe(undefined);
        });

        it('Should return an invalid validation if terms ask an object to has an specific structur and it does not respect', () => {
            const toValid: {object: {id: string, name: string, comments: string}} = {object: {id: 'some string', name: '', comments: undefined}};
            const subTerms: Validatable<{id: string, name: string, comments: string}> = {
                id: {type: ContractType.Number},
                name: {notEmpty: true, type: ContractType.String},
                comments: {required: true, type: ContractType.String},
            };
            const terms: Validatable<{object: {id: string, name: string, comments: string}}> = {object: {required: true, notEmpty: true, type:  ContractType.Object, content: subTerms}};

            const validation: Validation<{object: {id: string, name: string, comments: string}}> = welshguard.validate(toValid, terms);

            expect(validation.isValid).toBe(false);
            expect(validation.errors.object).toEqual({
                id: ['Le champ doit être un nombre.'],
                name: ['Le champ est requis.'],
                comments: ['Le champ est requis.'],
            });
        });

        it('Should return a valid validation if terms ask arrays elements to has an specific structur and it respect', () => {
            const toValid: {array: {id: string}[]} = {array: [{id: 'some string'}, {id: 'another string'}]};
            const subTerms: Validatable<{id: string}> = {id: {required: true, notEmpty: true, type: ContractType.String}};
            const terms: Validatable<{array: {id: string}[]}> = {array: {type:  ContractType.Array, content: subTerms}};

            const validation: Validation<{array: {id: string}[]}> = welshguard.validate(toValid, terms);

            expect(validation.errors).toBe(undefined);
            expect(validation.isValid).toBe(true);
        });
    });
});
