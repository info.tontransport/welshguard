import { isPhoneNumber } from './Validation';

describe('Validation', () => {
    describe('isPhoneNumber', () => {
        it('Should return true if value is string of 10 numbers', () => {
            const phone = '1231231234';
            const isValid = isPhoneNumber(phone);

            expect(isValid).toBe(true);
        });

        it('Should return true if value is string of 11 numbers', () => {
            const phone = '11231231234';
            const isValid = isPhoneNumber(phone);

            expect(isValid).toBe(true);
        });

        it('Should return false if value is string with a character', () => {
            const phone = '1123123123b';
            const isValid = isPhoneNumber(phone);

            expect(isValid).toBe(false);
        });
    });
});
