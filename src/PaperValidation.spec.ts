import {ContractType, PaperValidation} from './PaperValidation';

describe('PaperValidation', () => {
    describe('respectType', () => {
        it('Should return a valid result if value is a phone number and the required type is phone number', () => {
            const paperValidation = new PaperValidation<string>({type: ContractType.Phone}, '(123) 123-1234', 'phone');

            const result = paperValidation.respectType().getResult();

            expect(result.isValid).toBe(true);
        });

        it('Should return an invalid result if value is not a phone number and the required type is phone number', () => {
            const paperValidation = new PaperValidation<string>({type: ContractType.Phone}, '(123) 123-123e', 'phone');

            const result = paperValidation.respectType().getResult();

            expect(result.isValid).toBe(false);
        });
    });
});

describe('ContractType', () => {
    it('Should contain string contract', () => {
        expect(ContractType.String).toBe('string');
    });

    it('Should contain number contract', () => {
        expect(ContractType.Number).toBe('number');
    });

    it('Should contain object contract', () => {
        expect(ContractType.Object).toBe('object');
    });

    it('Should contain array contract', () => {
        expect(ContractType.Array).toBe('array');
    });

    it('Should contain phone contract', () => {
        expect(ContractType.Phone).toBe('phone');
    });

    it('Should contain email contract', () => {
        expect(ContractType.Email).toBe('email');
    });

    it('Should contain boolean contract', () => {
        expect(ContractType.Boolean).toBe('boolean');
    });
});
