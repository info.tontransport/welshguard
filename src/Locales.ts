export type DynamicString = (...attrs: (string | number)[]) => string;

export const getIsRequiredErrorMessage: DynamicString = (): string => {
    return `Le champ est requis.`;
};

export const getShouldNotBeDefinedErrorMessage: DynamicString = (): string => {
    return `Le champ doit être indéfinit.`;
};

export const getShouldNotBeEmptyErrorMessage: DynamicString = (): string => {
    return `Le champ est requis.`;
};

export const getShouldBeEmptyErrorMessage: DynamicString = (): string => {
    return `Le champ doit être vide.`;
};

export const attrNotEmailFormatError: DynamicString = (): string => {
    return `Le champ doit être un email.`;
};

export const getMustBeMinThatErrorMessage: DynamicString = (min: number): string => {
    return `La valeur minimale du champ est ${min}.`;
};

export const getMustBeAnObjectErrorMessage: DynamicString = (field: string): string => {
    return `Le champ ${field ? field + ' ' : ''}doit être un object.`;
};

export const getMustBeAnArrayErrorMessage: DynamicString = (field: string): string => {
    return `Le champ ${field ? field + ' ' : ''}doit être un tableau.`;
};

export const getMustBeAStringErrorMessage: DynamicString = (field: string): string => {
    return `Le champ ${field ? field + ' ' : ''}doit être une chaîne de caractères.`;
};

export const getMustBeANumberErrorMessage: DynamicString = (field: string): string => {
    return `Le champ ${field ? field + ' ' : ''}doit être un nombre.`;
};

export const getMustBeABooleanErrorMessage: DynamicString = (field: string): string => {
    return `Le champ ${field ? field + ' ' : ''}doit être un boolean.`;
};

export const getMustBeAnEmailErrorMessage: DynamicString = (field: string): string => {
    return `Le champ ${field ? field + ' ' : ''}doit être un courriel.`;
};

export const getMustBeAPhoneErrorMessage: DynamicString = (field: string): string => {
    return `Le champ ${field ? field + ' ' : ''}doit être un numéro de téléphone.`;
};
