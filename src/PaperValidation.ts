import { getIsRequiredErrorMessage, getMustBeABooleanErrorMessage, getMustBeAnArrayErrorMessage, getMustBeAnEmailErrorMessage, getMustBeAnObjectErrorMessage, getMustBeANumberErrorMessage, getMustBeAStringErrorMessage, getMustBeMinThatErrorMessage, getShouldNotBeEmptyErrorMessage, getMustBeAPhoneErrorMessage } from './Locales';
import { UnArray, ValidationState } from './Types';
import { hasMin, isArray, isBoolean, isDefined, isEmail, isEmpty, isNotDefined, isNumber, isObject, isString, isPhoneNumber } from './Validation';

export enum ContractType {
    String = 'string',
    Number = 'number',
    Array = 'array',
    Object = 'object',
    Email = 'email',
    Phone = 'phone',
    Boolean = 'boolean',
}
export interface Contract<T> {
    type: ContractType;
    required?: boolean;
    notEmpty?: boolean;
    min?: number;
    content?: Validatable<UnArray<T>>;
}

export type Validatable<T> = {[P in keyof T]: Contract<T[P]>};

export class PaperValidation<T> {
    private result: ValidationState<T> = {isValid: true};
    private get shouldValidate(): boolean {
        return this.result.isValid && (this.contract.required || isDefined(this.object));
    }
    public get unhandledRuleType(): string {
        return `Unhandled type: ${this.contract.type}`;
    }

    public constructor(private contract: Contract<T>, private object: T, contractName?: string) {}

    public respectRequiredStatement(): PaperValidation<T> {
        if (this.shouldValidate && isNotDefined(this.object)) {
            this.result.isValid = false;
            this.result.error = getIsRequiredErrorMessage();
        }
        return this;
    }

    public respectEmptyStatement(): PaperValidation<T> {
        if (this.shouldValidate && this.contract.notEmpty && isEmpty(this.object)) {
            this.result.isValid = false;
            this.result.error = getShouldNotBeEmptyErrorMessage();
        }
        return this;
    }

    public respectMinStatement(): PaperValidation<T> {
        if (this.shouldValidate && isDefined(this.contract.min) && !hasMin(this.object, this.contract.min)) {
            this.result.isValid = false;
            this.result.error = getMustBeMinThatErrorMessage(this.contract.min);
        }
        return this;
    }

    public respectType(): PaperValidation<T> {
        if (!this.shouldValidate) {
            return this;
        }
        const validationCondition = this.getTypeValidationCondition();
        if (!validationCondition.validateIs(this.object)) {
            this.result.isValid = false;
            this.result.error = validationCondition.message;
        }
        return this;
    }

    public getResult(): ValidationState<T> {
        return this.result;
    }

    private getTypeValidationCondition(): {validateIs: (val: any) => boolean, message: string} {
        switch (this.contract.type) {
            case ContractType.String:
                return {
                    validateIs: isString,
                    message: getMustBeAStringErrorMessage(),
                };
            case ContractType.Number:
                return {
                    validateIs: isNumber,
                    message: getMustBeANumberErrorMessage(),
                };
            case ContractType.Array:
                return {
                    validateIs: isArray,
                    message: getMustBeAnArrayErrorMessage(),
                };
            case ContractType.Object:
                return {
                    validateIs: isObject,
                    message: getMustBeAnObjectErrorMessage(),
                };
            case ContractType.Email:
                return {
                    validateIs: isEmail,
                    message: getMustBeAnEmailErrorMessage(),
                };
            case ContractType.Phone:
                return {
                    validateIs: isPhoneNumber,
                    message: getMustBeAPhoneErrorMessage(),
                };
            case ContractType.Boolean:
                return {
                    validateIs: isBoolean,
                    message: getMustBeABooleanErrorMessage(),
                };
            default:
                throw new Error(this.unhandledRuleType);
        }
    }
}
