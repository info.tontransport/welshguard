import * as _ from 'underscore';

export const reduce = <T, N, P extends keyof T>(object: T, callback: (old: N, value: T[P], key: P) => N, newObject: N): N => _.reduce(
    object as any,
    callback as any,
    newObject,
);
