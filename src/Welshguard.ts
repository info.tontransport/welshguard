import { NextFunction, Request, RequestHandler, Response } from 'express';
import * as _ from 'underscore';
import { Contract, PaperValidation, Validatable } from './PaperValidation';
import { Errors, Validation } from './Types';
import { reduce } from './Utils';
import { isArray, isDefined } from './Validation';

export class Welshguard {
    public UnhandleRule = (rule: keyof Contract<any>) => `Unhandled rule: ${rule}`;

    public getBodyGuard<T>(object: Validatable<T>): RequestHandler {
        return (req: Request, res: Response, next: NextFunction) => {
            const validation: Validation<T> = this.validate(req.body, object);
            validation.isValid ? next() : res.status(422).send(validation);
        };
    }

    public validate<T extends object>(body: T, terms: Validatable<T>): Validation<T> {
        const objectToValid = (isArray(body) ? body : [body]) as T[];

        for (const element in objectToValid) {
            return this.getValidation(objectToValid[element], terms);
        }

        return {isValid: true};
    }

    private getValidation<T extends any, P extends keyof T>(element: T, terms: Validatable<T>): Validation<T> {
        return reduce<Validatable<T>, Validation<T>, P>(
            terms,
            (old, rules, key) => {
                const ruleValidation = this.parseRules(element[key], rules, key as string);
                if (!ruleValidation.isValid) {
                    return {
                        isValid: false,
                        errors: {
                            ...old.errors as object,
                            [key]: ruleValidation.errors,
                        } as Errors<T>,
                    };
                } else {
                    return old;
                }
            },
            {isValid: true},
        );
    }

    private parseRules<T extends object, P extends string>(value: T, rules: Contract<T>, key: P): Validation<T> {
        const validationState: {isValid: boolean, errors?: Errors<T>} = {isValid: true};

        if (rules.required || isDefined(value)) {
            _.every(_.keys(rules), (rule: keyof Contract<T>) => {
                let errors: Errors<T>;
                let isValid: boolean;
                if (rule === 'content') {
                    const validation = this.validate(value, rules[rule] as Validatable<T>);
                    isValid = validation.isValid;
                    errors = validation.errors;
                } else {
                    const validationState = new PaperValidation(rules, value, key)
                        .respectRequiredStatement()
                        .respectType()
                        .respectEmptyStatement()
                        .respectMinStatement()
                        .getResult();
                    isValid = validationState.isValid;
                    errors = [validationState.error] as any;
                }
                if (!isValid) {
                    validationState.isValid = false;
                    validationState.errors = errors;
                }
                return validationState.isValid;
            });
        }
        return validationState;
    }
}
