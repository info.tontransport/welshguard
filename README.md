# welshguard.js

Welshguard is a validation library for fontend and backend (Express.js).

### Installing
```
$ npm i --save welshguard
```

## Backend (Express.js)

```javascript
const app = express();
createServer(app).listen(3000);

const middleware = new Welshguard().getBodyGuard({
    name: {required: true, notEmpty: true, type: RuleTypes.String},
});

app.post('/', middleware, (req, res) => {
    res.send('hello ' + req.body.name);
});
```
