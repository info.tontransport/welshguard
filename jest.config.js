module.exports = {
    setupFilesAfterEnv: ["<rootDir>/node_modules/jest-enzyme/lib/index.js"],
    setupFiles: [
        "<rootDir>/test-shim.js",
    ],
    transform: {
        "^.+\\.tsx?$": "ts-jest",
    }, // Override default babel transpilation (it improve performance)
    moduleFileExtensions: [
        "js",
        "ts",
        "tsx",
    ],
    testMatch: [
        "<rootDir>/src/**/**.spec.ts",
    ],
    testURL: 'http://test/',
}